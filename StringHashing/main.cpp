#include "StringHasher.hpp"
#include "CrudeStringClass.h"

//For cout etc.
#include <iostream>

//For timing.
#include <chrono>

//for strings
#include <string>
#include <fstream>
#include <sstream>
#include <cassert>
#include <unordered_map>

using std::string;
using std::ifstream;
using std::ofstream;
using std::cout;
using std::endl;
using std::to_string;
using std::stringstream;

struct TimingData 
{
public:
    float hashTime;
    float arrayTime;
    TimingData() 
    {
        this->hashTime = 0;
        this->arrayTime = 0;
    }  
    TimingData(float hashTime, float arrayTime)
    {
        this->hashTime = hashTime;
        this->arrayTime = arrayTime;
    }
};

int main(int argc, char* argv[])
{

    /* CONSTANTS AND VARIABLES */
    const int PROGRAM_ITERATIONS = 20;      // how many times to run the outer loop
    const int DATA_LINES = 25000;           // how many lines of data to store
    const int ITERATIONS = 100;             // how many iterations to run inner loop
    int step_size = DATA_LINES / ITERATIONS;// how many lines we should increment each iteration
    int start_pos = step_size - 1;          // Where to start :)

    // Array for storing our results
   // int factorArray[PROGRAM_ITERATIONS][ITERATIONS];
    float timeArray[PROGRAM_ITERATIONS][ITERATIONS];

    // Get a ref to the string hasher singleton
    StringHasher& bob = StringHasher::getInstance();

    // Initialise our crude string class
    CrudeStringClass crudeness = CrudeStringClass();
    assert(DATA_LINES == crudeness.ARRAY_SIZE); // lazy ;)

    // C++ 11 Hash Map
    std::unordered_map<string, string> hashmap;

    // Vars for searching and results
    stringstream ss;
    nlist* result;
    string crude_result;
    string search_term;

    // Vars for reading data from file
    string myText;
    int count = 0; 
    ifstream MyReadFile("complete works shakespeare.txt");

    // vars for filewriting
    string filepath = "output.csv";

    ////////////////////////////////////////////////////////////////////////////////////////////////



    
    /* SETUP */

    // Load the data into the string array and the hash table!
    while (crudeness.getPosition() < crudeness.ARRAY_SIZE)
    {
        getline(MyReadFile, myText);

        // prune out empty lines!
        while (strcmp(myText.c_str(), "") == 0)            
            getline(MyReadFile, myText);
        
        myText += "\t[";
        myText += to_string(count);  // to ensure all lines are unique
        myText += "]";

        crudeness.addString(myText);
        bob.install(myText);
       
    }

   

    ////////////////////////////////////////////////////////////////////////////////////////////////




    /* BEGIN TESTING */
    for (int h = 0; h < PROGRAM_ITERATIONS; ++h)
    {
        // Line number. We declare this here since it needs to reset with outer loop
        int line_number = start_pos;

        for (int i = 0; i < ITERATIONS; ++i)
        {
            search_term = crudeness.getString(line_number);


            // we first search the hash table            
            auto start = std::chrono::high_resolution_clock::now();     // Record start time
         
            result = bob.lookup(search_term);                           // Perform search
           
            auto finish = std::chrono::high_resolution_clock::now();    // Stop the clock!
            
            std::chrono::duration<double> elapsed = finish - start;     // calculate time elapsed


            // now searching the array of strings....       
            auto start_crude = std::chrono::high_resolution_clock::now();           // start the clock
      
            crude_result = crudeness.findString(search_term);                       // perform search 
         
            auto finish_crude = std::chrono::high_resolution_clock::now();          // stop the clock!
         
            std::chrono::duration<double> elapsed_crude = finish_crude - start_crude;// calculate time elapsed


            // Didnt find the string. Quit!
            if (strcmp(crude_result.c_str(), search_term.c_str()) != 0 || result == NULL)
            {
                cout << "ERROR! ";
                return -1;
            }
           
            int factor = elapsed_crude / elapsed;   // How many times faster is hash table lookup?
            timeArray[h][i] = elapsed_crude.count();             // Save result to table
            /*
            if (factor < 1)
            {
                cout << line_number << "\t" << crudeness.getString(line_number) << endl;
                cout << std::fixed << "\t" << "hash: " << elapsed.count() << ", arr: " << elapsed_crude.count() << endl;
            }
            */

            cout << std::fixed << elapsed.count() << " | " << elapsed_crude.count() << endl;
            

            line_number += step_size;              // Increment line number
        }        
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////




    /* SAVE RESULTS TO FILE */   
    ofstream output(filepath);

    // This looks weird, but we want to print as many rows as the number of inner loop iterations
    // Lots of rows tends to work better than lots of columns!
    for (int i = 0; i < ITERATIONS; ++i)
    {
        string str = "";

        for (int j = 0; j < PROGRAM_ITERATIONS; ++j)
        {
            str += to_string(timeArray[j][i]); 
            str += ",";            
        }

        str += "\n";

        output << str;
    }

    output.close();

    ////////////////////////////////////////////////////////////////////////////////////////////////




    return 0;
}



/*
    bob.install("this is a sentence");
    bob.install("a different sentence");
    bob.install("two words");
    bob.install("all day i dream about sporks");
    bob.install("another one for the record");
    bob.install("this ones for the dead");
    bob.install("MMMM MMMM MMMM MMMM");
    bob.install("42 my friend");
    bob.install("going loco down in sainsburys local");
    bob.install("epstein didnt kill himself");
    bob.install("end of the list");
*/