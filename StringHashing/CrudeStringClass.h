#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

class CrudeStringClass 
{
public:
	const static int ARRAY_SIZE = 25000;

	CrudeStringClass();
	~CrudeStringClass();

	string findString(string s);
	bool findStringFaster(string s);

	int getPosition();

	void addString(string s);

	string getString(int index);

	void printStrings();

private:

	int position;

	string arrayOfStrings[ARRAY_SIZE];
};