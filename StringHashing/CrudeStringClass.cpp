#include "CrudeStringClass.h"


CrudeStringClass::CrudeStringClass()
{
	position = 0;
}

CrudeStringClass::~CrudeStringClass()
{
}

string CrudeStringClass::findString(string s)
{
	for (int i = 0; i < ARRAY_SIZE; ++i)
	{
		//cout << "comparing \"" << arrayOfStrings[i] << "\" with \"" << s << "\"" << endl;

		if (strcmp(arrayOfStrings[i].c_str(), s.c_str()) == 0)
		{
			//cout << "found " << s << endl;
			return arrayOfStrings[i];
		}
	}

	return "NOT FOUND";
}

bool CrudeStringClass::findStringFaster(string s)
{
	for (int i = 0; i < ARRAY_SIZE; ++i)
	{
		if (strcmp(arrayOfStrings[i].c_str(), s.c_str()) == 0)
		{
			return true;
		}
	}

	return false;
}

int CrudeStringClass::getPosition()
{
	return position;
}

void CrudeStringClass::addString(string s)
{
	arrayOfStrings[position] = s;
	++position;
}

string CrudeStringClass::getString(int index)
{
	return arrayOfStrings[index];
}

void CrudeStringClass::printStrings()
{
	for (int i = 0; i < ARRAY_SIZE; ++i)
	{
		cout << i << ":\t>" << arrayOfStrings[i] << endl;
		//cout << arrayOfStrings[i] << endl;
	}
}


