#include <iostream>

class Game
{
    private:
    //Note constructor and destructor are private
    Game();
    ~Game();
    //This is the one staic object, essentially a singleton. 
    //Note that this has to be initalised outside the header file. 
    static Game c_game;

    public:
    static Game& getInstance();

    void test();
};    
       

    
    
