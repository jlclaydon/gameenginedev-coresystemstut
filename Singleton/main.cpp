/* SINGLETON

Creating a Singlton to test the ideas in Object-Orientated Games Development by Julian Gold, the singleton class here is called Game 

To compile:  Use premake4 to configure the project as in the premake4.lua file, build files for a range of environments can be created (see premake4 --help)

*/

#include <iostream>
#include "Game.hpp"

int main(int argc, char* argv[])
{
    std::cout << "Hello World, Hello Josh" << std::endl;
    Game& game = Game::getInstance();

    game.test();
}

