#include "StackAllocator.h"

//For cout etc. 
#include <iostream>
#include <string>

//For malloc and comparision
#include <cstdlib>

//For timing. 
#include <chrono>

#include <fstream>
#include <sstream>

#define KB 1024
#define MB 1024*1024

using std::string;
using std::to_string;

using std::cout;
using std::endl;

void testAllocation();
void timeAllocation();

int main(int argc, char* argv[])
{
    //testAllocation();
    timeAllocation();

    return 0;
}

void testAllocation()
{
    const int ARRAY_B_MAX = 10;
    const int ARRAY_C_MAX = 100;

    int *anIntArray;
    int *anotherIntArray;

    StackAllocator& stack = StackAllocator::getInstance();
    stack.init(MB * 500);

    cout << "Print the marker, this is the top of the stack" << endl;
    cout << stack.getMarker() << endl;

    //Allocate a block
    anIntArray = (int*)stack.alloc(sizeof(int)*10);
    cout << stack.getMarker() << endl;

    //Allocate another block. 
    anotherIntArray = (int*)stack.alloc(sizeof(int)*10);
    cout << stack.getMarker() << endl;

    //Make sure block works
    for(int i=0; i<10; ++i)
        anIntArray[i] = i;

    for(int i=0; i<10; ++i)
        cout << anIntArray[i] << endl;

    //Output stuff
    cout << "Size of int:" << sizeof(int) << endl;

    cout << "anIntArray ptr value:" << anIntArray << endl;
    cout << "anotherIntArray ptr value" << anotherIntArray << endl;

    
}

struct AllocationTime
{
    int megaBytes = 0;
    long double time = 0;
    long double time2 = 0;
    AllocationTime() {}
    AllocationTime(int megaBytes, long double time, long double time2)
    {
        this->megaBytes = megaBytes;
        this->time = time;
        this->time2 = time2;
    }
};

void timeAllocation()
{   
    const int PROGRAM_ITERATIONS = 256;
    const unsigned int MAX_ALLOC = 1 * KB;
    const int BLOCK_SIZE = sizeof(int);
    const unsigned int NUMBER_OF_BLOCKS = MAX_ALLOC / BLOCK_SIZE;
    const int STEP = NUMBER_OF_BLOCKS / PROGRAM_ITERATIONS;

    cout << NUMBER_OF_BLOCKS;    

    StackAllocator& stack = StackAllocator::getInstance();

    AllocationTime timings[PROGRAM_ITERATIONS];


    for (int i = 1; i <= PROGRAM_ITERATIONS; ++i)
    {
        int blocks_to_alloc = STEP * i;
        unsigned int mem = blocks_to_alloc * BLOCK_SIZE;
        int memKB = mem / KB;
        int memMB = memKB / KB;
        
      //  blocks_to_allocate += stepSize;

        // Record start time
        auto start = std::chrono::high_resolution_clock::now();

        int* fred = (int*)stack.alloc(sizeof(int) * blocks_to_alloc);

        // Record end time
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<long double> elapsed = finish - start;
        std::cout << "Stack allocator (" << blocks_to_alloc << " ints, " << memKB << " KB) time: " << elapsed.count() << " s\n";


        // Record start time
        auto start2 = std::chrono::high_resolution_clock::now();

        int* bob = (int*)malloc(sizeof(int) * blocks_to_alloc);

        // Record end time
        auto finish2 = std::chrono::high_resolution_clock::now();
        std::chrono::duration<long double> elapsed2 = finish2 - start2; 
        std::cout << "Malloc \t\t(" << blocks_to_alloc << " ints, " << memKB << " KB) time: " << elapsed.count() << " s\n";

        timings[i-1] = AllocationTime(memMB, elapsed.count(),elapsed2.count());


        free(bob);
    }

    for (int i = 0; i < PROGRAM_ITERATIONS; ++i)
    {
        cout << timings[i].megaBytes << " MB : " << timings[i].time << endl;
    }


    /* SAVE RESULTS TO FILE */
    std::ofstream output("allocation_timing.csv");

    for (int i = 0; i < PROGRAM_ITERATIONS; ++i)
    {
        std::stringstream ss;



        ss << timings[i].megaBytes;
        ss << ",";
        ss << timings[i].time;
        ss << ",";
        ss << timings[i].time2;
        ss << "\n";

        output << ss.str();
    }

    output.close();

}
