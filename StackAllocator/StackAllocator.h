#include <cstdlib>
#include <cstdint>

class StackAllocator
{

public:
	//Not part of Gregory's interface (but implied). 
	typedef intptr_t U32;
	typedef intptr_t Marker;


	//Explicit keyword ensures this is not used
        //as a conversion constructor. 
	explicit StackAllocator(U32 stackSize_bytes);	

	virtual ~StackAllocator();

	StackAllocator();

	void init(U32 stackSize_bytes);

	//Allocate a block (return start). 
	void* alloc(U32 size_bytes);
	
	//Get stack top (a marker). 
	Marker getMarker();

	//Unroll the stack to the marker given. 
	void freeToMarker(Marker marker);

	//Unroll the whole stack. 
	void clear();

	static StackAllocator& getInstance();

private:
	Marker marker;
	Marker currMarker;
	static StackAllocator singleton;
};
