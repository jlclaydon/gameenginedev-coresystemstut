#ifndef _GAMEOBJECTFACTORY_HPP_
#define _GAMEOBJECTFACTORY_HPP_

#include "GameObject.hpp"


class GameObjectFactory
{
    public:
	GameObjectFactory();
	~GameObjectFactory();

        static GameObject* create(GameObject::Type eType);
};

#endif

